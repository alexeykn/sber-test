import os.path, time, logging
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from flask import Flask, request
from flask_restful import Resource, Api


FILES = []

app = Flask(__name__)
api = Api(app)



class MyEventHandler(PatternMatchingEventHandler):
    def on_moved(self, event):
        super(MyEventHandler, self).on_moved(event)
        logging.info("File %s was just moved" % event.src_path)

    def on_created(self, event):
        super(MyEventHandler, self).on_created(event)
        logging.info("File %s was just created" % event.src_path)
        update_key(event.src_path)

    def on_deleted(self, event):
        super(MyEventHandler, self).on_deleted(event)
        logging.info("File %s was just deleted" % event.src_path)

    def on_modified(self, event):
        super(MyEventHandler, self).on_modified(event)
        logging.info("File %s was just modified" % event.src_path)
        update_key(event.src_path)

class files(Resource):
    def post(self):
        print(request.get_json()['path'])
        path = request.get_json()['path']
        FILES.append({'path': path, 'last_line': -1})
        watch_files(path)
        update_key(path)
        return {'status': 'success'}

    def get(self):
        res = []
        for file in FILES:
            res.append({file['path']: file['key']})
        return res



api.add_resource(files, '/files')


def seek_errors(path, last_line):
    search_phrase = 'ERROR'
    key = 0
    with open(path, "r") as f:
        searchlines = f.readlines()
    for i, line in enumerate(searchlines):
        if i > last_line:
            if search_phrase in line:
                key += 1
            last_line = i
    return {'path': path, 'last_line': last_line, 'key': key}


def update_key(path):
    for i in range(0, len(FILES)):
        if FILES[i]['path'] == path:
            res = seek_errors(path, FILES[i]['last_line'])
            FILES[i]['key'] = res['key']



def watch_files(path=None):
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    observer = Observer()
    threads = []
    #for path in paths:
    #--
    file_path = path.strip()
    watched_dir = os.path.split(file_path)[0]
    print('watched_dir = {watched_dir}'.format(watched_dir=watched_dir))
    patterns = [file_path]
    print('patterns = {patterns}'.format(patterns=', '.join(patterns)))
    event_handler = MyEventHandler(patterns=patterns)

    observer.schedule(event_handler, watched_dir, recursive=True)
    threads.append(observer)
    #--
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()


if __name__ == "__main__":
    app.run()
